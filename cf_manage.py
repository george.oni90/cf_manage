#!/usr/bin/env python

import sys, os, json, re
import requests, argparse
import socket
import CloudFlare

from argparse import RawTextHelpFormatter

def getCredentials():
    parser = argparse.ArgumentParser(
                     description='CloudFlare python wrapper used to create, update and delete DNS records. Connection is done using user and token.',
                     prog='cf_manage',
		     formatter_class=RawTextHelpFormatter,
                     epilog='Usage examples:\n'
                            ' Creating/Updating an entry:\n'
			    '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --name mydomain --ip 1.1.1.1 --type A --proxy\n'
			    '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --name mydomain1 --ip 192.168.0.1 --proxy\n'				
			    '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --name mydomainalias --ip alias-gigi.mydomain.com --type CNAME --proxy\n'
			    '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --name mydomainalias1 --ip alias1-gigi.mydomain.com\n'
		            ' Updating master zone IP:\n'
                            '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --ip 1.1.1.2 --proxy\n'
                            '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --autoapp\n'
			    ' Delete an entry:\n'
			    '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --name mydomain_to_delete --delete\n'
			    '   cf_manage --username user@myzone.com --passworcd pasteyourtokenhere123 --zone myzone.com --name mydomain_to_delete -d\n'
		            )
    group = parser.add_mutually_exclusive_group()
    parser.add_argument('--username',help='CF Username - email format', required=True, metavar='USER', dest='user')
    parser.add_argument('--password',help='CF Password Token', required=True, metavar='ApiTOKEN', dest='token')
    parser.add_argument('--zone',help='CF DNS Zone', required=True, metavar='DNSZONE', dest='zone')
    parser.add_argument('--name',help='DNS name to be updated or created',metavar='NAME', dest='dnsname')
    group.add_argument('--ip',help='IP/CNAME for the DNS name', metavar='IP-NAME',dest='ip')
    parser.add_argument('--type',help='Rregistry type: A, AAAA, CNMAE etc',dest='type',choices=['A','AAAA','CNAME'])
    parser.add_argument('-p','--proxy',help='Allow CF to proxy DNS and WEB traffic',dest='proxy',action='store_true')
    group.add_argument('-a','--autoapp',help='CF Update with local Public IP',dest='autoapp',action='store_true')
    group.add_argument('-d','--delete',help='CF Remove Domain (USE WITH CAUTION)',dest='delete',action='store_true')
    return parser.parse_args()


def getMyIP_Public():

    # This list is adjustable - plus some v6 enabled services are needed
    # url = 'http://myip.dnsomatic.com'
    # url = 'http://www.trackip.net/ip'
    # url = 'http://myexternalip.com/raw'
    url = 'https://api.ipify.org'
    try:
        ip_address = requests.get(url).text
    except:
        exit('%s: failed' % (url))
    if ip_address == '':
        exit('%s: failed' % (url))

    if ':' in ip_address:
        ip_address_type = 'AAAA'
    else:
        ip_address_type = 'A'

    return ip_address, ip_address_type

def getCFdata(email,token):
   return  CloudFlare.CloudFlare(email=email, token=token)

def getCFzoneID(cf,credentials):
   try:
     cf_zones = cf.zones.get()
   except CloudFlare.exceptions.CloudFlareAPIError as e:
     exit('/zones.get %d %s - api call failed' % (e, e))
   except Exception as e:
        exit('/zones.get - %s - api call failed' % (e))
   #Check if account has no zones configured
   if len(cf_zones) == 0:
        print('Error! No Zones foun on account (%s)' % (credentials.user))
        exit(1)
   #Validate zone existance
   for data in cf_zones:
       if data['name'] == credentials.zone:
          return data['id']
          check=True
       else:
          check=False
   if check==False: 
       print('Zone: %s : is not a valid zone on the specified account (%s)' % (credentials.zone,credentials.user))
       exit(1)

def updateCFdnsRecord(cf,credentials,dns_id,zone_id):
   dns_record = {
       'name':credentials.domain,
       'type':credentials.type,
       'content':credentials.ip,
       'proxied':credentials.proxy
       }
   try:
     dns_record = cf.zones.dns_records.put(zone_id,dns_id,data=dns_record)
   except CloudFlare.exceptions.CloudFlareAPIError as e:
      exit('/zones.dns_records.put %s - %d %s - api call failed' % (credentials.domain, e, e))

def createCFdnsRecord(cf,credentials,zone_id):
   dns_record = { 
       'name':credentials.domain,
       'type':credentials.type,
       'content':credentials.ip,
       'proxied':credentials.proxy
      }
   try:
     dns_record = cf.zones.dns_records.post(zone_id,data=dns_record)
   except:
     exit('/zones.dns_records.post %s - %d %s - api call failed' % (credentials.domain, e, e))
      
def CFdnsRecord(cf,credentials):
   zone_id = getCFzoneID(cf,credentials)
   try:
     dns_records = cf.zones.dns_records.get(zone_id)
   except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones/dns_records.get %d %s - api call failed' % (e, e))
   for data in dns_records:
       if data['name'] == credentials.domain:
          if data['content'] == credentials.ip and data['proxied'] == credentials.proxy:
             print('DNS-CF-INFO: Domain %s already up-to date: %s -- NO CHANGES WILL be made' % (credentials.domain,credentials.ip) )
             exit(0)
          else:
            updateCFdnsRecord(cf,credentials,data['id'],zone_id)
            print('DNS-CF-INFO: Domain %s changed from %s to %s' % (credentials.domain,data['content'],credentials.ip) )
            exit(0)
   createCFdnsRecord(cf,credentials,zone_id)
   print('DNS-CF-INFO: Domain %s created to %s' % (credentials.domain,credentials.ip) )

def deleteCFdnsRecord(cf,credentials):
   zone_id = getCFzoneID(cf,credentials)
   try:
        dns_records = cf.zones.dns_records.get(zone_id)
   except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones/dns_records.get %d %s - api call failed' % (e, e))
   for data in dns_records:
         if data['name'] == credentials.domain:
            try: 
               cf.zones.dns_records.delete(zone_id,data['id'])
            except CloudFlare.exceptions.CloudFlareAPIError as e:
               exit('/zones/dns_records.get %d %s - api call failed' % (e, e))
            print('DNS-CF-INFO: Domain %s has been DELETED !' % (credentials.domain) )

def is_valid_ipv4_address(address):
    try:
        socket.inet_pton(socket.AF_INET, address)
    except AttributeError:  # no inet_pton here, sorry
        try:
            socket.inet_aton(address)
        except socket.error:
            return False
        return address.count('.') == 3
    except socket.error:  # not a valid address
        return False

    return True

def is_valid_ipv6_address(address):
    try:
        socket.inet_pton(socket.AF_INET6, address)
    except socket.error:  # not a valid address
        return False
    return True

def main():
   #Get Argumetns
   credentials = getCredentials()
   cf = getCFdata(credentials.user,credentials.token)
   if credentials.dnsname is None:
      credentials.domain = credentials.zone
   else:
      credentials.domain = (credentials.dnsname+'.'+credentials.zone)
   #Delete Record 
   if credentials.delete == True:
      deleteCFdnsRecord(cf,credentials)
      exit(0)
   #SOA update conditions
   if credentials.autoapp == True:
      ip_addr, ip_addr_type = getMyIP_Public()
      credentials.ip = ip_addr
      credentials.type = ip_addr_type
   #Validate IP type
   if credentials.type is None:
     if is_valid_ipv4_address(credentials.ip) == True:
        credentials.type = "A"
     if is_valid_ipv6_address(credentials.ip) == True:
        credentials.type = "AAAA"
     else:
        credentials.type = "CNAME" 
   CFdnsRecord(cf,credentials)
   exit(0)
if __name__ == '__main__':
    main()
